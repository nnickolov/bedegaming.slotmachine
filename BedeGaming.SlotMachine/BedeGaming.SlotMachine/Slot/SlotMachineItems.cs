﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BedeGaming.SlotMachine.Slot
{
    public class SlotMachineItems<T>
    {
        public double Probability { get; set; }
        public T Item { get; set; }
    }
}
