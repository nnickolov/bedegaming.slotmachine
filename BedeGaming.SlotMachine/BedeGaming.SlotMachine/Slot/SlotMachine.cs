﻿using BedeGaming.SlotMachine.CapitalHandlers;
using System;

namespace BedeGaming.SlotMachine
{
    public static class SlotMachine
    {

        public static void Run()
        {
            var capitalHandler = new CapitalHandler();
            var capital = capitalHandler.GetInitialBalance();
            capitalHandler.Gamble(capital);
        }
    }
}
