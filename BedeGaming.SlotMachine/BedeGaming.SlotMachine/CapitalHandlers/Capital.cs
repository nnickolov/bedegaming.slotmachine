﻿namespace BedeGaming.SlotMachine.CapitalHandlers
{
    public class Capital 
    {
        public Capital(double deposit)
        {
            this.Deposit = deposit;
        }
        public double Deposit { get; set; }
    }
}