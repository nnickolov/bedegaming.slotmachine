﻿using BedeGaming.SlotMachine.Slot;

using System.Collections.Generic;

namespace BedeGaming.SlotMachine.CapitalHandlers
{
    public interface ICapitalHandler
    {
        public Capital GetInitialBalance();

        public void Gamble(Capital capital);

        public List<SlotMachineItems<string>> CalculationPossibilities();

        double Print(List<SlotMachineItems<string>> slotMachineItems);

        double Sum(string currentLine);
    }
}
