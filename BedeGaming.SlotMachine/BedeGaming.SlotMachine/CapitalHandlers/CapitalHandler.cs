﻿using BedeGaming.SlotMachine.CapitalHandlers;
using BedeGaming.SlotMachine.Slot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BedeGaming.SlotMachine.CapitalHandlers
{
    public class CapitalHandler : ICapitalHandler
    {
        public Capital GetInitialBalance()
        {
            Console.WriteLine("Please deposit money you would like to play with:");

            var deposit = double.Parse(Console.ReadLine());

            var capital = new Capital(deposit);

            return capital;
        }

        public void Gamble(Capital capital)
        {
            Console.WriteLine("Enter stake amount:");

            var stake = double.Parse(Console.ReadLine());

            Console.WriteLine();

            if (stake > capital.Deposit)
            {
                Console.WriteLine($"Invalid stake! Current amount: {capital.Deposit} is less then the current stake: {stake}!");
                Gamble(capital);
            }

            var calculationPossibilities = CalculationPossibilities();
            var coefficient = Print(calculationPossibilities);

            capital.Deposit = (capital.Deposit - stake) + (coefficient * stake);

            Console.WriteLine($"You have won: {(coefficient * stake):F2}");
            Console.WriteLine($"Current balance is : {capital.Deposit:F2}");
            Console.WriteLine();

            if (capital.Deposit <= 0)
            {
                Console.WriteLine("Game over!");
                Environment.Exit(0);
            }
            else
            {
                Gamble(capital);
            }
        }

        public List<SlotMachineItems<string>> CalculationPossibilities()
        {
            var slotMachineItems = new List<SlotMachineItems<string>>
                                   {
                                       new SlotMachineItems<string> {Probability = 45 / 100.0, Item = "A"},
                                       new SlotMachineItems<string> {Probability = 35 / 100.0, Item = "B"},
                                       new SlotMachineItems<string> {Probability = 15 / 100.0, Item = "P"},
                                       new SlotMachineItems<string> {Probability = 5 / 100.0, Item = "*"},
                                   };

            var converted = new List<SlotMachineItems<string>>(slotMachineItems.Count);
            var sum = 0.0;
            foreach (var item in slotMachineItems.Take(slotMachineItems.Count - 1))
            {
                sum += item.Probability;
                converted.Add(new SlotMachineItems<string> { Probability = sum, Item = item.Item });
            }

            converted.Add(new SlotMachineItems<string> { Probability = 1.0, Item = slotMachineItems.Last().Item });

           return converted;
        }

        public double Print(List<SlotMachineItems<string>> slotMachineItems)
        {
            var rnd = new Random();

            var rows = 4;
            var cols = 3;

            string[,] table = new string[rows, cols];

            double coefficient = 0;

            for (int i = 0; i < rows; i++)
            {
                var currentLine = string.Empty;
                for (int k = 0; k < cols; k++)
                {
                    var probability = rnd.NextDouble();

                    var selected = slotMachineItems.SkipWhile(i => i.Probability < probability).First();
                    currentLine += selected.Item + " ";
                    Console.Write(table[i, k] = selected.Item);
                }
                Console.WriteLine();
                coefficient += Sum(currentLine.TrimEnd());
            }

            Console.WriteLine();

            return coefficient;
        }

        public double Sum(string currentLine)
        {
            var line = currentLine.Split(" ");

            double sum = 0;

            if (line[0] == "A")
            {
                sum = 0.4;
                if (line[1] != "A" && line[1] != "*")
                    sum = 0;
                else if (line[2] != "A" && line[2] != "*")
                    sum = 0;
                else
                {
                    if (line[1] == "A")
                        sum += 0.4;
                    if (line[2] == "A")
                        sum += 0.4;
                }
            }
            else if (line[0] == "B")
            {
                sum = 0.6;
                if (line[1] != "B" && line[1] != "*")
                    sum = 0;
                else if (line[2] != "B" && line[2] != "*")
                    sum = 0;
                else
                {
                    if (line[1] == "B")
                        sum += 0.6;
                    if (line[2] == "B")
                        sum += 0.6;
                }
            }
            else if (line[0] == "P")
            {
                sum = 0.8;
                if (line[1] != "P" && line[1] != "*")
                    sum = 0;
                else if (line[2] != "P" && line[2] != "*")
                    sum = 0;
                else
                {
                    if (line[1] == "P")
                        sum += 0.8;
                    if (line[2] == "P")
                        sum += 0.8;
                }
            }
            else if (line[0] == "*")
            {
                sum = 0.0;
                if (line[1] != "*" && line[2] != "*")
                {
                    if (line[1] != line[2])
                        sum = 0;
                }
                else if (line[1] == "*" && line[2] != "*")
                {
                    if (line[2] == "A")
                    {
                        sum += 0.4;
                    }
                    if (line[2] == "B")
                    {
                        sum += 0.6;
                    }
                    if (line[2] == "P")
                    {
                        sum += 0.8;
                    }
                }
                else if (line[1] != "*" && line[2] == "*")
                {
                    if (line[1] == "A")
                    {
                        sum += 0.4;
                    }
                    if (line[1] == "B")
                    {
                        sum += 0.6;
                    }
                    if (line[1] == "P")
                    {
                        sum += 0.8;
                    }
                }
                else if (line[1] == line[2])
                {
                    if (line[1] == "A")
                    {
                        sum += 0.8;
                    }
                    if (line[1] == "B")
                    {
                        sum += 1.2;
                    }
                    if (line[1] == "P")
                    {
                        sum += 1.6;
                    }
                }
            }
            return sum;
        }
    }
}